---
layout: news
title: Second project plenary meeting
date: 2023-09-18
image: /assets/images/news/logo-campus-cyber.png
---

The second plenary meeting of the SecurEval project was held on 18th September at Campus Cyber near Paris.