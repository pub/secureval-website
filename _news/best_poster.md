---
layout: news
title: Best poster award at GDR GPL 2023
date: 2023-06-05
image: /assets/images/news/best_poster.png
---

Best poster award was received at GDR GPL 2023 for works on incremental safety analysis with CEA and Sorbonne Université.
