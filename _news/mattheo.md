---
layout: news
title: Polytechnique's award for research internship
date: 2022-12-01
---

Matthéo Vergnolles was awarded for his research internship at CEA for his work on relational analysis and hidden channels.