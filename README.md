This is an empty website project to serve as a basis for creating a new website.

The website is based on Jekyll [https://jekyllrb.com/] and uses a CSS framework Bulma [https://bulma.io/] to simplify CSS creation.

# Dependencies for website building (optional)

The website uses Jekyll, a static website generator written
in Ruby. Install Ruby as well as the `bundler` package
management according to your setting.

To install all necessaries dependencies to build and serve locally the website:
```
bundle config set --local path 'vendor/bundle'
bundle install
```

To serve the website locally:
```
bundle exec jekyll serve --incremental
```
A localhost address (usually, localhost:4000) will then
appear in your shell. Go there with your web browser, and
the website should be fully rendered.
Modify at will the content or CSS, changes will be made apparent in real-time.

# Content config

Some configuration of the content is available in _config.yml. It can be modified at will and used in the rest of the website through the tags "site.element". 
The navigation bar items are defined here as well as the logo and some other elements.

# CSS

As mentioned the CSS uses the Bulma framework, which proposes a lot of different elements ready and easy to use. Bulma files are downloaded locally in /assets/css/bulma.sass. We use scss in order to be able to define some color or other in the css file. The custom colors can be defined in /assets/css/custom.css, you can modify the base colors of bulma (i.e. the ones mentioned in the doc, primary, success) or add new colors in /assets/css/custom.scss.
You can also add some css option in /assets/css/main.scss. Some basic CSS formatting is already present in that file.


# Current deployment

The website is served through the CI pipeline and directly uses Gitlab Pages to put it online. The website address can be found in the Settings of the project while the CI is in `.gitlab-ci.yml`.
